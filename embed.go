package versioninfo

import "embed"

var (
	//go:embed docs/api-generated
	DocsAPIGenerated embed.FS

	//go:embed api
	API embed.FS

	//go:embed assets/generated/swagger
	Swagger embed.FS
)
