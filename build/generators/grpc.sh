#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")

PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..
THIRD_PARTY_LIBRARY_API=${PROJECT_DIRECTORY}/third_party/
GRPC_GATEWAY_LIBRARY_API=${PROJECT_DIRECTORY}/third_party/
#GRPC_GATEWAY_LIBRARY_API=${PROJECT_DIRECTORY}/vendor/github.com/grpc-ecosystem/grpc-gateway

# Version v1 stuff
VERSION_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/version/v1
VERSION_V1_PROTOBUF_FILE_API=${VERSION_V1_PROTOBUF_DIRECTORY_API}/gitlab_com_mvenezia_versionv1.proto
VERSION_V1_DESTINATION_LIBRARY_API=grpc/version/v1
VERSION_V1_DESTINATION_LOCATION_API=${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}
VERSION_V1_SWAGGER_DESTINATION=${PROJECT_DIRECTORY}/assets/generated/swagger/version/v1
VERSION_V1_DOCS_DESTINATION=${PROJECT_DIRECTORY}/docs/api-generated/version/v1

DOCKER_ARGUMENTS="run --rm=true -it -v ${PWD}:/code -w /code --entrypoint /usr/bin/protoc"
DOCKER_IMAGE=quay.io/venezia/grpc-golang:v3.21.12

echo "Ensuring Version API Destination Directory ( ${VERSION_V1_DESTINATION_LOCATION_API} ) Exists..."
mkdir -p ${VERSION_V1_DESTINATION_LOCATION_API}

echo "Ensuring Version Swagger Destination Directory ( ${VERSION_V1_SWAGGER_DESTINATION} ) Exists..."
mkdir -p ${VERSION_V1_SWAGGER_DESTINATION}

echo "Ensuring Version Documentation Destination Directory ( ${VERSION_V1_DOCS_DESTINATION} ) Exists..."
mkdir -p ${VERSION_V1_DOCS_DESTINATION}

echo
echo "generating version v1 api stubs..."
echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ --go_out=./${VERSION_V1_DESTINATION_LOCATION_API}/ --go_opt=paths=source_relative --go-grpc_out=paths=source_relative:./${VERSION_V1_DESTINATION_LOCATION_API}/ --grpc-gateway_out=logtostderr=true,paths=source_relative:./${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}/ --openapiv2_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION} --doc_out=${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md -I${THIRD_PARTY_LIBRARY_API} -I${GRPC_GATEWAY_LIBRARY_API}"
docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/  --go_out=./${VERSION_V1_DESTINATION_LOCATION_API}/ --go_opt=paths=source_relative --go-grpc_out=paths=source_relative:./${VERSION_V1_DESTINATION_LOCATION_API}/ --grpc-gateway_out=logtostderr=true,paths=source_relative:./${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}/ --openapiv2_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION} --doc_out=${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md -I${THIRD_PARTY_LIBRARY_API} -I${GRPC_GATEWAY_LIBRARY_API}

#echo
#echo "generating version v1 REST gateway stubs..."
#echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --grpc-gateway_out=logtostderr=true:${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}"
#docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --grpc-gateway_out=logtostderr=true,paths=source_relative:./${PROJECT_DIRECTORY}/${VERSION_V1_DESTINATION_LIBRARY_API}/
#
#echo
#echo "generating version v1 swagger docs..."
#echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --swagger_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION}"
#docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --openapiv2_out=logtostderr=true,allow_merge=true,merge_file_name=api:${VERSION_V1_SWAGGER_DESTINATION}
#
#echo
#echo "generating version v1 api docs..."
#echo "protoc ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out=${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md"
#docker $DOCKER_ARGUMENTS $DOCKER_IMAGE ${VERSION_V1_PROTOBUF_FILE_API} -I ${VERSION_V1_PROTOBUF_DIRECTORY_API}/ -I ${THIRD_PARTY_LIBRARY_API} --doc_out ${VERSION_V1_DOCS_DESTINATION} --doc_opt=markdown,api.md
