package cmd

import (
	"flag"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"strings"
)

var (
	rootCmd = &cobra.Command{
		Use:   "version-sample",
		Short: "Version Sample CLI",
		Long:  `Why are you reading this?`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("You probably want to try version command")
			os.Exit(1)
		},
	}
)

func init() {
	viper.SetEnvPrefix("versionsample")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
