# version-info library

Library that can pull out common version information

Useful for Web APIs or CLIs really

# Example Usage

## CLI

See [this](samples/cli/cmd/version.go) example of how you could use the library in a [cobra](https://github.com/spf13/cobra)
powered application

You can test it out by doing this:
```bash
GO111MODULE=on go run samples/cli/main.go version
```

And you should get output similar to:
```bash
$ go run samples/cli/main.go version
2019/03/19 23:29:20 Version Requested
Version Information:
        Git Data:
                Tagged Version:
                Hash:           62b773e2d894f86116556c6b854dcb5e8d2111c9
                Tree State:     dirty
        Build Data:
                Build Date:
                Go Version:     go1.12
                Compiler:       gc
                Platform:       windows/amd64

2019/03/19 23:29:20 Version Served
```

## Web API

See [this protobuf](samples/grpc/api/api.proto) file and [this implementation](samples/grpc/version.go) 
of the generated server.

You can peruse through the [build](samples/grpc/build) folder to see how things were generated


# Misc Details

Thanks for looking!